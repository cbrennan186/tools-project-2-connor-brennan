using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;


public class ZombieUXMLEditor : EditorWindow
{
    ZombieGrunt grunt;

    [MenuItem("Tools/ZombieUXMLEditor")]
    public static void ShowExample()
    {
        ZombieUXMLEditor wnd = GetWindow<ZombieUXMLEditor>();
        wnd.titleContent = new GUIContent("ZombieUXMLEditor");
    }

    public void CreateGUI()
    {
        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Editor/UI Toolkit - UXML/ZombieUXMLEditor.uxml");
        var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Editor/UI Toolkit - UXML/ZombieUXMLEditor.uss");

        VisualElement editor = visualTree.Instantiate();
        editor.styleSheets.Add(styleSheet);
        root.Add(editor);

        ObjectField zombieObjectField = editor.Q<ObjectField>("ZombieObjectFieldSelector");
        zombieObjectField.objectType = typeof(ZombieGrunt);
        zombieObjectField.RegisterCallback<ChangeEvent<Object>>(OnZombieObjectChanged);

        EnumField zombieState = editor.Q<EnumField>("ZombieState");
        zombieState.Init(Zombie.States.None);
    }

    private void OnZombieObjectChanged(ChangeEvent<Object> evt)
    {
        grunt = evt.newValue as ZombieGrunt;
        if (grunt != null)
        {
            SerializedObject serializedObject = new SerializedObject(grunt);
            rootVisualElement.Bind(serializedObject);
        }
        else
        {
            rootVisualElement.Unbind();
        }
    }
}