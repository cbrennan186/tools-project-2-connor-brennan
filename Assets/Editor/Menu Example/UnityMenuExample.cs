using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class UnityMenuExample : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("Root Menu/Do Something")]
    static void DoSomething()
    {
        // Which one?
        if (Selection.activeGameObject)
        {
            Debug.Log($"Active game object selected: {Selection.activeGameObject.name}");
        }

        if (Selection.activeObject)
        {
            Debug.Log($"Active object selected: {Selection.activeObject.name}");
        }

        if (Selection.activeObject == null && Selection.activeGameObject == null)
        {
            Debug.Log($"Nothing Selected");
        }
    }
#endif
}
