using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class UnityFileWritingExample : MonoBehaviour
{
    // Add a function to write the transform data for the selected game object
    [MenuItem("Root Menu/Export Data")]
    static void ExportGameObjectTransform()
    {
        string path = Application.dataPath + "/Temp.txt";
        using(StreamWriter writer = new StreamWriter(path))
        {
            writer.WriteLine(Selection.activeGameObject.transform.position);
            writer.WriteLine(Selection.activeGameObject.transform.rotation);
            writer.WriteLine(Selection.activeGameObject.transform.localScale);
            writer.Close();
        }

        //AssetDatabase.ImportAsset(path);
    }

    // Validate that a game object is selected
    [MenuItem("Root Menu/Export Data", true)]
    static bool ValidateExportGameObjectTransform()
    {
        return (Selection.activeGameObject == null ? false : true);
    }
}
