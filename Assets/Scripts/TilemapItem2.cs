using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TilemapItem2", menuName = "Game Data/TilemapItem2")]
[System.Serializable]
public class TilemapItem2 : TilemapItem
{
    public bool isHidden = true;
    public string filePath = "";
    public int numberOfTiles = 25;
}
