using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ZombieGrunt", menuName = "Game Data/Zombie Grunt")]
public class ZombieGrunt : Zombie
{
    public bool showBackStory = false;
    public string backStory;
    public int lives = 0;
    public float attackDamage = 0.0f;
}
