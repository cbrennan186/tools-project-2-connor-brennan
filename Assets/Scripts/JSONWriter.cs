using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class JSONWriter : MonoBehaviour
{
    [System.Serializable]
    public class TilemapJSONFile
    {
        public string fileName = "Test";
    }

    public TilemapJSONFile tilemapJSONFile = new TilemapJSONFile();

    public void printJSON()
    {
        string newJsonfile = JsonUtility.ToJson(tilemapJSONFile);

        File.WriteAllText(Application.dataPath + "/Temp12.txt", newJsonfile);
    }
}
