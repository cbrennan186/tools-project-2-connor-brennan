using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Zombie", menuName = "Game Data/Zombie")]
public class Zombie : ScriptableObject
{
    public enum States
    {
        None,
        Idle,
        Attack,
        Dead
    };

    //public string name; (can we use the base class?)
    public float health = 100.0f;
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;
    public States startingState = States.None;
}
